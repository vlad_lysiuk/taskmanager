class Task < ActiveRecord::Base
  belongs_to :list
  
  validates :content, presence: true, length: { minimum:6 }
  validates :content, uniqueness: {message: "has already been used"}

  def uncompleted?
    !completed
  end

  # Extract options_for_select array from the view just for clarity
  def self.priority_options
    [['Very high', 1], ['High', 2], ['Normal', 3],['Low', 4]]
  end

  def print_priority
    case priority
      when 1
        "Very high"
      when 2
        "High"
      when 3
        "Normal"
      when 4
        "Low"
      else
        "Not specified"
      end
  end

  def print_deadline
    deadline ? deadline.strftime("Complete before %d/%m/%Y %n | %H:%M") : "None"
  end
end
