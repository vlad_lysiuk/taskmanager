class List < ActiveRecord::Base
  belongs_to :user
  has_many :tasks, dependent: :destroy
  
  validates :title, presence: true, length: { minimum: 4 }

  def completed?
    status = true
    tasks.each do |task|
      if task.uncompleted?
        status = false
      end
    end
    status
  end

end
