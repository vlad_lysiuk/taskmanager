class TasksController < ApplicationController
  before_action :set_related_list, only: [:new, :create]
  before_action :set_task, only: [:edit, :update, :destroy, :set_completed]
  before_action :authenticate_user!

  def new
    if @related_list.user_id != current_user.id
      flash[:alert] = "Nice try, but you can't add task to the list, that doesn't belongs to you"
      redirect_to lists_path
    else
      @task = @related_list.tasks.build
    end
  end

  def create
    @task = @related_list.tasks.build(task_params)
    if @task.save
      flash[:notice] = "Task was successfully created"
      redirect_to lists_path
    else
      flash.now[:alert] = "Something wrong with you data. Try again"
      render :new
    end
  end

  def edit
    # Check task for completion just for reliability. Though we hide edit link
    # when task has been completed, user can type the request manually. Also check if
    # this task belongs to the user's list.
    if @task.list.user_id != current_user.id
      flash[:alert] = "Nice try, but you can't edit task, that doesn't belongs to your list"
      redirect_to lists_path
    elsif @task.completed?
      flash[:alert] = "Sorry, pal, but you can't edit completed task"
      redirect_to lists_path
    end
  end

  def update
    if @task.update_attributes(task_params)
      flash[:notice] = "Task was successfully updated"
      redirect_to lists_path
    else
      flash.now[:alert] = "Something wrong with you data. Try again"
      render :new
    end
  end

  def destroy
    if @task.completed?
      flash[:alert] = "Sorry, but you can't delete completed task"
    else
      @task.destroy
      flash[:notice] = "Task was successfully deleted"
    end
    redirect_to lists_path
  end

  def set_completed
    if @task.completed?
      flash[:alert] = "Sorry, this task has been completed before"
    else
      @task.update_attribute(:completed, true)
      flash[:notice] = "Task was completed. Great job!"
    end
    redirect_to lists_path
  end

  private

    def set_related_list
      @related_list = List.find(params[:list_id])
    end

    def set_task
      @task = Task.find(params[:id])
    end

    def task_params
      params.require(:task).permit(:content, :deadline, :priority, :list_id)
    end
end
