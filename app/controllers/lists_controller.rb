class ListsController < ApplicationController
  before_action :set_list, only: [:edit, :update, :destroy]
  before_action :authenticate_user!

  def index
    @lists = current_user.lists
  end

  def new
    @list = current_user.lists.build
  end

  def edit
    if @list.user_id != current_user.id
      flash[:alert] = "Nice try, but you can't edit list, which doesn't belongs to you"
      redirect_to lists_path
    end
  end

  def create
    @list = current_user.lists.build(list_params)
    if @list.save
      flash[:notice] = "List was successfully created."
      redirect_to lists_path
    else
      flash.now[:alert] = "Something wrong with you data. Try again"
      render :new
    end
  end

  def update
    if @list.update_attributes(list_params)
      flash[:notice] = "List was successfully updated."
      redirect_to lists_path
    else
      flash.now[:alert] = "Something wrong with you data. Try again"
      render :new
    end
  end

  def destroy
    if @list.completed?
      @list.destroy
      flash[:notice] = "List was successfully destroyed."
    else
      flash[:alert] = "List can not be removed because contains uncompleted tasks"
    end
    redirect_to lists_path
  end

  private

    def set_list
      @list = List.find(params[:id])
    end

    def list_params
      params.require(:list).permit(:title)
    end
end
