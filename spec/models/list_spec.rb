require 'rails_helper'

RSpec.describe List, type: :model do
  it { should have_many(:tasks)}
  it { should belong_to(:user)}

  describe "completed?" do
    let!(:list) { List.create(title: "Complete test task for Ruby Garage") }
    let!(:task_1) {list.tasks.create(content: "Write HTML & CSS", priority: 2)}
    let!(:task_2) {list.tasks.create(content: "Another task", priority: 3, completed: true)}

    it "is false when list has uncompleted tasks" do
      expect(list.completed?).to be false
    end

    it "is true when all list tasks completed" do
      task_1.update_attribute(:completed, true)
      expect(list.completed?).to be true
    end
  end
end
