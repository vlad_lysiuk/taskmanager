require 'rails_helper'

RSpec.describe Task, type: :model do
  it { should belong_to(:list) }

  let!(:list) { List.create(title: "Complete test task for Ruby Garage") }
  let!(:task) {list.tasks.create(content: "Write HTML & CSS", priority: 2, deadline: Time.now + 30.minutes)}

  describe "print_priority" do
    it "is return correct text equivalent for integer value" do
      expect(task.print_priority).to eq "High"
    end
  end

  describe "print_deadline" do
    it "is return format string if deadline is set up" do
      expect(task.print_deadline).to match(/Complete before(.*)/)
    end
  end
end
