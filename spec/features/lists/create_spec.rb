require 'rails_helper'

describe "Create TODO list" do
  let!(:user) {User.create(email: "test@example.com", password: "foobar")}

  before(:each) do
    login_as(user)
  end

  it "redirect to the todo lists index page on success" do
    create_todo_list
    expect(page).to have_content("Default")
  end

  it "displays an error when todo list has no title" do
    create_todo_list(title: "")
    expect(page).to have_content("error")
    expect(List.count).to eq(0)
  end

  it "displays an error when todo list has too short title (less then 4 characters)" do
    create_todo_list(title: "Com")
    expect(page).to have_content("error")
    expect(List.count).to eq(0)

    visit "/lists"
    expect(page).to_not have_content("Com")
  end

end
