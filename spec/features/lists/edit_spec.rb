require 'rails_helper'

describe "Edit TODO list" do

  let!(:user) {User.create(email: "test@example.com", password: "foobar")}
  let!(:list) {user.lists.create(title: "Complete test task for Ruby Garage")}

  before(:each) do
    login_as(user)
  end

  it "updates todo list succesfully with correct information" do
    update_todo_list list: list
    # Fetch most recent info from database
    list.reload
    expect(page).to have_content("List was successfully updated.")
    expect(list.title).to eq("Default title for todo list")
  end

  it "displays an error when updating without title" do
    update_todo_list list: list, title: ""
    list.reload

    # List title should not be updated
    expect(list.title).to eq("Complete test task for Ruby Garage")
    expect(page).not_to have_content("List was successfully updated.")
    expect(page).to have_content("error")
  end

  it "displays an error when updating with too short title" do
    update_todo_list list: list, title: "Zzz"
    list.reload

    expect(list.title).to eq(list.title)
    expect(page).not_to have_content("List was successfully updated.")
    expect(page).to have_content("error")
  end

end
