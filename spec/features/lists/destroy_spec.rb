require 'rails_helper'

describe "Destroy TODO list" do

  let!(:user) {User.create(email: "test@example.com", password: "foobar")}
  let!(:list) {user.lists.create(title: "Complete test task for Ruby Garage") }
  let!(:task) {list.tasks.create(content: "Another task", priority: 2)}

  before(:each) do
    login_as(user)
  end

  it "succesful destroy list when click on link" do
    visit '/lists'
    task.update_attribute(:completed, true)

    within "#list_#{list.id}" do
      click_link "Delete todo list"
    end

    expect(page).to have_content("List was successfully destroyed.")
    expect(page).not_to have_content(list.title)
    expect(List.count).to eq(0)
  end

  it "does not destroy list with uncompleted tasks" do
    visit '/lists'

    within "#list_#{list.id}" do
      click_link "Delete todo list"
    end

    expect(list.completed?).to be false
    expect(page).to have_content("List can not be removed because contains uncompleted tasks")
    expect(List.count).to eq(1)
  end
end
