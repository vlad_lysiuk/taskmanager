require 'rails_helper'

describe "Destroy task for todo list" do

  let!(:user) {User.create(email: "test@example.com", password: "foobar")}
  let!(:list) {user.lists.create(title: "Complete test task for Ruby Garage") }
  let!(:task) {list.tasks.create(content: "Another task", priority: 2)}

  before(:each) do
    login_as(user)
  end

  it "succesful destroy task when click on link" do
    visit '/lists'

    within "#task_#{task.id}" do
      click_link "Delete"
    end

    expect(page).not_to have_content(task.content)
    expect(Task.count).to eq(0)
  end
end
