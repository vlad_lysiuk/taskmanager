require 'rails_helper'

describe "Create tasks for todo list" do

  let!(:user) {User.create(email: "test@example.com", password: "foobar")}
  let!(:list) {user.lists.create(title: "Complete test task for Ruby Garage") }
  let!(:task) {list.tasks.create(content: "Another task", priority: 2)}

  before(:each) do
    login_as(user)
  end

  it "successful with valid data" do
    create_task
    expect(page).to have_content("Task was successfully created")

    within (".task_list") do
      expect(page).to have_content("Default task content")
    end
  end

  it "displays an error with no content" do
    create_task(content: "")
    expect(page).to have_content("Something wrong with you data. Try again")
    expect(page.all(".task_list tr").size).to eq(0)

    within "#error_explanation ul" do
      expect(page).to have_content("can't be blank")
    end
  end

  it "displays an error with too short content (less than 6 chars)" do
    create_task(content: "Short")
    expect(page).to have_content("Something wrong with you data. Try again")
    expect(page.all(".task_list tr").size).to eq(0)

    within "#error_explanation ul" do
      expect(page).to have_content("too short")
    end
  end

  it "displays an error with repeated content" do
    create_task(content: "Another task")
    expect(page).to have_content("Something wrong with you data. Try again")
    expect(page.all(".task_list tr").size).to eq(0)

    within "#error_explanation ul" do
      expect(page).to have_content("has already been used")
    end
  end

end
