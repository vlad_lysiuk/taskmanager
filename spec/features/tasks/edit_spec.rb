require 'rails_helper'

describe "Edit tasks for todo list" do
  let!(:user) {User.create(email: "test@example.com", password: "foobar")}
  let!(:list) {user.lists.create(title: "Complete test task for Ruby Garage") }
  let!(:task) {list.tasks.create(content: "Another task", priority: 2)}
  let!(:foo_task) {list.tasks.create(content: "Lorem Ipsum", priority: 3)}

  before(:each) do
    login_as(user)
  end

  it "is successful when update with valid content" do
    update_task task: task
    expect(page).to have_content("Task was successfully updated")
    task.reload

    within (".task_list") do
      expect(page).to have_content("Default task content")
    end
  end

  it "display an error when update with no content" do
    update_task task: task, content: ""
    expect(page).not_to have_content("Task was successfully updated")
    expect(page).to have_content("Something wrong with you data. Try again")
    task.reload

    expect(task.content).to eq("Another task")
    within "#error_explanation ul" do
      expect(page).to have_content("can't be blank")
    end
  end

  it "display an error when update with too short content" do
    update_task task: task, content: "Short"

    expect(page).not_to have_content("Task was successfully updated")
    expect(page).to have_content("Something wrong with you data. Try again")
    task.reload
    expect(task.content).to eq("Another task")

    within "#error_explanation ul" do
      expect(page).to have_content("too short")
    end
  end

  it "display an error when update with repeated content" do
    update_task(task: foo_task, content: "Another task")
    expect(page).not_to have_content("Task was successfully updated")
    expect(page).to have_content("Something wrong with you data. Try again")

    foo_task.reload
    expect(foo_task.content).to eq("Lorem Ipsum")

    within "#error_explanation ul" do
      expect(page).to have_content("has already been used")
    end
  end

end
