require 'rails_helper'

describe "Viewing tasks for todo list" do

  let!(:user) {User.create(email: "test@example.com", password: "foobar")}
  let!(:list) {user.lists.create(title: "Complete test task for Ruby Garage") }

  before(:each) do
    login_as(user)
  end

  it "displays the title of todo list" do
    visit "/lists"
    expect(page).to have_content("Complete test task for Ruby Garage")
  end

  it "display no tasks when todo list is empty" do
    visit "/lists"
    expect(page.all("tbody.task_list tr").size).to eq(0)
  end

  it "display content when todo list has tasks" do
    list.tasks.create(content: "Open mockup in Adobe Fireworks", deadline: Time.now + 1.days)
    list.tasks.create(content: "Write HTML & CSS", deadline: Time.now + 2.days)

    visit "/lists"

    within ".task_list" do
      expect(page).to have_content("Open mockup in Adobe Fireworks")
      expect(page).to have_content("Write HTML & CSS")
    end

    expect(page.all("tbody.task_list tr").size).to eq(2)

  end
end
