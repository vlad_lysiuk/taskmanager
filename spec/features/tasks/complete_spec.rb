require 'rails_helper'

describe "Mark task as done" do

  let!(:user) {User.create(email: "test@example.com", password: "foobar")}
  let!(:list) {user.lists.create(title: "Complete test task for Ruby Garage") }
  let!(:task) {list.tasks.create(content: "Another task", priority: 2)}

  before(:each) do
    login_as(user)
  end

  it "succesful when marking a single task complete" do
    visit '/lists'

    within "#task_#{task.id}" do
      click_link "Mark as complete"
    end

    task.reload
    expect(task.completed).to be true
    expect(page).to have_content("Task was completed. Great job!")
    expect(page).not_to have_content("Mark as complete")
  end
end
