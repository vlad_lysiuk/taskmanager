module TaskHelpers
  def create_task(options={})

    options[:content] ||= "Default task content"
    options[:deadline] ||= Time.now + 2.hours
    options[:priority] ||= "High"

    visit "/lists"
    within "#list_#{list.id}" do
      click_link "Add new task"
    end

    fill_in "Content", with: options[:content]
    fill_in "Set up deadline (not necessary)", with: options[:deadline]
    select options[:priority], from: "Select priority for your task"

    click_button "Create Task"
  end

  def update_task(options={})

    task = options[:task]
    options[:content] ||= "Default task content"
    options[:deadline] ||= Time.now + 2.hours
    options[:priority] ||= "High"

    visit "/lists"
    within "#task_#{task.id}" do
      click_link "Edit"
    end

    fill_in "Content", with: options[:content]
    fill_in "Set up deadline (not necessary)", with: options[:deadline]
    select options[:priority], from: "Select priority for your task"

    click_button "Update Task"
  end

end
