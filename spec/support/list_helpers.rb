module ListHelpers
  def create_todo_list(options={})
    # Conditional assignment: if we do not pass any arguments, title equal to "Default title.."
    # Otherwise, options[:title] is equal to appropriate argument
    options[:title] ||= "Default title for todo list"
    visit "/lists"
    click_button "Add new TODO list"
    expect(page).to have_content("New List")

    fill_in "Title", with: options[:title]
    click_button "Create List"
  end

  def update_todo_list(options={})

    options[:title] ||= "Default title for todo list"
    list = options[:list]

    visit '/lists'

    within "#list_#{list.id}" do
      click_link "Edit todo list"
    end

    fill_in "Title", with: options[:title]
    click_button "Update List"
  end
end
